;;; -*- Syntax: Common-lisp; Package: clim-user -*-

(defpackage :clim-treelist.filesystem-viewer
  (:use :clim :clim-lisp :clim-treelist)
  (:export
   #:view-files
   #:choose-files
   #:browse-files))

(in-package :clim-treelist.filesystem-viewer)

;;;************************************************************

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; An application for viewing file directories. 
;;;;
;;;; Pretty much straight from the original software.  Really, just a
;;;; function call atop the application frame.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun directory-p (pathname)
  (not (pathname-name pathname)))

(defun directory-name (pathname)
  (let ((list (pathname-directory pathname)))
    (when (consp list)
      (string (car (last list))))))

(defun file-name (pathname)
  (file-namestring pathname))

;;
;; The "essential-group" class that is openable/closable
;;

(defclass directory-display
    (essential-group)
    ((pathname :initarg :pathname :accessor encapsulated-pathname)
     (contents :accessor group-contents)))

(defmethod print-object ((self directory-display) stream)
  ;; (break "FOO")
  (format stream "#<directory-display ~A>" (group-name self)))

(defmethod group-name ((self directory-display))
  (directory-name (encapsulated-pathname self)))

(defmethod group-contents :around ((self directory-display))
  (if (not (slot-boundp self 'contents))
      (let* ((stuff (cl-fad:list-directory (encapsulated-pathname self))))
        (setf (group-contents self)
          (append (mapcar
                   #'(lambda (p)
                       (make-instance 'directory-display
                         :pathname (cl-fad:pathname-directory-pathname p)))
                   (sort (remove-if-not #'directory-p stuff)
                         #'string-lessp :key #'directory-name))
                  (sort (mapcar #'file-name
                                (remove-if #'directory-p stuff))
                        #'string-lessp))))
    (call-next-method self)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; File chooser
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;
;; The leaf node to display - a wrapped pathname
;;

(defclass selectable-pathname ()
  ((my-pathname :initarg :my-pathname :initform NIL :accessor my-pathname)
   (selected-p :initform nil :accessor selected-p)
   (needs-redraw-p :initform t :initarg :needs-redraw-p :accessor needs-redraw-p)))

(defmethod directoryp ((self selectable-pathname))
  (not (pathname-name (my-pathname self))))

(defmethod dir-name ((self selectable-pathname))
  (let ((list (pathname-directory (my-pathname self))))
    (when (consp list)
      (string (car (last list))))))

(defmethod f-name ((self selectable-pathname))
  (file-namestring (my-pathname self)))

#+NIL
(defmethod print-object ((self selectable-pathname) stream)
  (format stream "SELECTABLE-PATHNAME#<~A>" (my-pathname self)))

;;
;; The non-leaf-node to display - a wrapped directory, with an
;; associated leaf node wrapped pathname
;;

(defclass selectable-dir-display (essential-group)
  ((pathname-obj :initarg :pathname-obj :accessor pathname-obj)
   (contents :accessor group-contents)))

(defmethod print-object ((self selectable-dir-display) stream)
  ;; (break "FOO")
  (format stream "#<selectable-dir-display ~A>" (group-name self)))

(defmethod group-name ((self selectable-dir-display))
  (pathname-obj self))

(defmethod list-directory ((self selectable-dir-display))
  (handler-case (mapcar #'(lambda (p)
			    (make-instance 'selectable-pathname :my-pathname p))
			(cl-fad:list-directory (my-pathname (pathname-obj self))))
		(t (var) (clouseau:inspect (list t var)) nil)))

(defmethod group-contents :around ((self selectable-dir-display))
  (if (not (slot-boundp self 'contents))
      (let* ((stuff (list-directory self)))
        (setf (group-contents self)
          (append (mapcar
                   #'(lambda (p)
                       (make-instance 'selectable-dir-display
                         :pathname-obj
			 #+NIL (uiop:pathname-directory-pathname p)
			 #-NIL p
			 )
		       )
                   (sort (remove-if-not #'directoryp stuff)
                         #'string-lessp :key #'dir-name))
		  #+NIL
                  (sort (mapcar #'f-name
				(remove-if #'directoryp stuff))
                        #'string-lessp)
		  #-NIL
		  (sort (remove-if #'directoryp stuff)
			#'string-lessp :key #'f-name)
		  )))
    (call-next-method self)))

;;
;; The presentation type of the leaf node
;;

(define-presentation-type selectable-pathname ())

(define-presentation-method present :around (object (type selectable-pathname) stream view &key)
  ;; (clouseau:inspect (list :present object))
  ;; (format *terminal-io* "before ~A~%" object)
  (cond ((directoryp object)
	 ;; (with-text-face (stream :roman) (write-string (dir-name (my-pathname object)) stream))
	 (with-drawing-options (stream :ink (if (selected-p object) +red+ +black+))
			       (with-text-face (stream :roman)
					       (write (dir-name object) :stream stream)))
	 )
	(t
	 (with-drawing-options (stream :ink (if (selected-p object) +red+ +black+))
			       (with-text-face (stream :italic)
					       (write (f-name object) :stream stream)))
	 )
	)
  ;; (call-next-method)
  ;; (format *terminal-io* "after ~A~%" object)
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;;  Filesystem chooser
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (trace group-contents)
;; (trace clim-treelist::display-or-redisplay-group)
;; (trace clim-treelist::display-viewer-group)
;; (trace redisplay-frame-pane)
;; (trace clim-treelist::display-indented-list)
;; (trace clim-treelist::display-viewer-group)
;; (trace clim-treelist::com-toggle-open)

(define-application-frame file-chooser (clim-treelist::group-viewer)
  ((chosen-files :initform nil :accessor chosen-files)
   (initial-root-dir :initform nil :initarg :initial-root-dir :accessor initial-root-dir))
  (:command-table (file-chooser :inherit-from (clim-treelist::group-viewer)))
  (:panes
   (display (scrolling ()
		       (make-pane 'application-pane
				  :display-function 'clim-treelist::display-viewer-group
				  :display-time :no-clear))))
  (:layouts
   (default (vertically () display))))

(defmethod clim:run-frame-top-level :around ((frame file-chooser) &rest args)
  (let ((directory (initial-root-dir frame)))
    (setf
     (clim-treelist::group-viewer-group frame)
     (make-instance
      'selectable-dir-display
      :pathname-obj (make-instance
		     'selectable-pathname
		     :my-pathname (cl-fad:pathname-directory-pathname directory))))
    (setf
     (clim-treelist::group-viewer-ptype frame)
     'selectable-pathname)
    (setf
     (clim-treelist::group-viewer-displayer frame)
     nil)
    (call-next-method)))

(define-file-chooser-command (com-done
			      :menu "Done"
			      :name "Done")
  ()
  (frame-exit *application-frame*))

(define-file-chooser-command (com-up
			      :menu "Up"
			      :name "Up")
  ()
  (let* ((af *application-frame*)
	 (gvg (clim-treelist::group-viewer-group af))
	 (pnameobj (pathname-obj gvg))
	 (pname (my-pathname pnameobj)))
    (clouseau:inspect (list :up af gvg pnameobj pname))
    ;;(clouseau:inspect gvg)
    ;;(clouseau:inspect pnameobj)
    ;;(clouseau:inspect pname)
    )
  )

#+NIL
(define-file-chooser-command (com-redisplay
			      :menu "Redisplay"
			      :name "Redisplay")
  ()
  (redisplay-frame-panes *application-frame*))
 
#+NIL
(define-file-chooser-command (com-inspect-frame
			      :menu "Inspect Frame"
			      :name "Inspect Frame")
  ()
  (clouseau:inspect *application-frame*))
 

(define-presentation-action com-toggle-selected (selectable-pathname command file-chooser
							     :gesture :select
							     :documentation "Toggle Selected" :menu t)
  (object window)
  ;; (clouseau:inspect (list :com-toggle-selected *application-frame* window))
  ;; (format *terminal-io* "object = ~A~%" object)

  (cond ((selected-p object)
	 ;; (format *terminal-io* "UNSELECTED ~S~%" object)
	 (setf (selected-p object) nil)
	 (setf
	  (chosen-files *application-frame*)
	  (remove
	   (my-pathname object)
	   (chosen-files *application-frame*)
	   :test #'equal)))
	(t
	 ;; (format *terminal-io* "SELECTED ~S~%" object)
	 (setf (selected-p object) t)
	 ;; (clouseau:inspect object)
	 (pushnew (my-pathname object) (chosen-files *application-frame*))))

  ;; (pushnew (my-pathname object) (chosen-files *application-frame*))

  (setf (needs-redraw-p object) t)
  (redisplay-frame-pane *application-frame* window))


#+NIL
(clim-treelist::define-group-viewer-command (com-toggle-selected)
  ((pname 'selectable-pathname :gesture :select))
  ;; (clouseau:inspect pname)
  (cond ((selected-p pname)
	 (format *terminal-io* "UNSELECTED ~S~%" pname)
	 (setf (selected-p pname) nil))
	(t
	 (format *terminal-io* "SELECTED ~S~%" pname)
	 (setf (selected-p pname) t)))
  (let* ((f *application-frame*)
	 (p (find-pane-named f 'clim-treelist::display)))
    (clouseau:inspect (list :redisplay f p))
    ;; (redisplay-frame-pane f p :force-p t)
    ;; (redisplay-frame-pane f 'clim-treelist::display :force-p t)
    )
  pname
  )

;;
;; Top-level driver function
;;

(defun view-files (directory)
  (view-group (make-instance 'directory-display
                :pathname (cl-fad:pathname-directory-pathname directory)
                :display-contents t)
	      'clim::pathname))

(defun choose-files (directory)
  (let* ((frame (make-application-frame 'file-chooser :initial-root-dir directory)))
    (run-frame-top-level frame)
    ;; (clouseau:inspect (chosen-files frame))
    (chosen-files frame)
    )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Filesystem viewer app
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun display-selection (frame stream)
  (when (current-selection frame)
    (let* ((my-pathname-obj (current-selection frame))
	   (pathname (my-pathname my-pathname-obj)))
      (write pathname :stream stream)
      (terpri stream)
      (write "Type: " :stream stream)
      (write (pathname-type pathname) :stream stream)
      (terpri stream)
      (write "Author: " :stream stream)
      (write (uiop/pathname::file-author pathname) :stream stream)
      (terpri stream)
      (write "Write date: " :stream stream)
;;      (write (uiop/pathname::file-write-date pathname) :stream stream)
      (local-time:format-timestring stream (local-time:universal-to-timestamp (uiop/pathname::file-write-date pathname)))
      (terpri stream)
;;      (write (uiop/pathname::file-length pathname) :stream stream)
;;      (terpri stream)
      )
    )
  )

(defun display-notifications (frame stream)
  (write (gensym "notification") :stream stream))

(define-application-frame filesystem-viewer (clim-treelist::group-viewer)
  ((current-selection :initform nil :accessor current-selection)
   (initial-root-dir :initform nil :initarg :initial-root-dir :accessor initial-root-dir))
  (:command-table (filesystem-viewer :inherit-from (clim-treelist::group-viewer)))
  (:panes
   (hierarchy (scrolling ()
			 (make-pane 'application-pane
				    :display-function 'clim-treelist::display-viewer-group
				    :display-time :no-clear)))
   (selection (scrolling ()
			 (make-pane 'application-pane
				    :display-function 'display-selection
				    :display-time :command-loop)))
   (notifications (scrolling ()
			     (make-pane 'application-pane
					:display-function 'display-notifications
					:display-time :command-loop)))
   (documentation :pointer-documentation))
  (:layouts
   (default (vertically ()
			(horizontally ()
				      hierarchy
				      (vertically () selection notifications))
			documentation))))

(defmethod clim:run-frame-top-level :around ((frame filesystem-viewer) &rest args)
  (let ((directory (initial-root-dir frame)))
    (setf (clim-treelist::group-viewer-group frame) (make-instance
						     'selectable-dir-display
						     :pathname-obj (make-instance
								    'selectable-pathname
								    :my-pathname (uiop/pathname:pathname-directory-pathname directory)))
	  (clim-treelist::group-viewer-ptype frame) 'selectable-pathname
	  (clim-treelist::group-viewer-displayer frame) nil)
    (call-next-method)))

(define-filesystem-viewer-command (com-show-properties)
  ((pname 'selectable-pathname :gesture :select))
  (setf (current-selection *application-frame*) pname)
  (redisplay-frame-panes *application-frame*))

(define-filesystem-viewer-command (com-exit
				   :menu "Exit"
				   :name "Exit")
  ()
  (frame-exit *application-frame*))

(defun browse-files (directory)
  (let* ((frame (make-application-frame 'filesystem-viewer :initial-root-dir directory)))
    (run-frame-top-level frame)))
    
	  
