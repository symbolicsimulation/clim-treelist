SBCL = sbcl
CCL = lx86cl64

EVAL_BROWSER = --eval '(ql:quickload :clim-treelist)' \
               --eval '(clim-treelist.filesystem-viewer:browse-files \#P"/")'
EVAL_CHOOSER =  --eval '(ql:quickload :clim-treelist)' \
                --eval '(clouseau:inspector (clim-treelist.filesystem-viewer:choose-files \#P"/tmp/"))'
EVAL_VIEWER = --eval '(ql:quickload :clim-treelist)' \
              --eval '(clim-treelist.filesystem-viewer:view-files \#P"/tmp/")'

sbcl-browser:
	$(SBCL) $(EVAL_BROWSER)

sbcl-chooser:
	$(SBCL) $(EVAL_CHOOSER)

sbcl-viewer:
	$(SBCL) $(EVAL_VIEWER)

ccl-browser:
	$(CCL) $(EVAL_BROWSER)

ccl-chooser:
	$(CCL) $(EVAL_CHOOSER)

ccl-viewer:
	$(CCL) $(EVAL_VIEWER)

COMPONENTS = ../asdf/uiop/ ../../dists/quicklisp/software/osicat-20130720-git/ ../mcclim 

tags:
	sbcl --eval "(ql:quickload :clim-treelist)" --eval "(make-tags :clim-treelist)"
