;;; -*- lisp -*-

(defpackage :clim-treelist.system
  (:use :cl :asdf))

(in-package :clim-treelist.system)

(defsystem :clim-treelist
  :depends-on (:mcclim :clouseau :cl-fad :uiop :local-time)
  :serial t
  :components
  ((:file "package")
   (:file "treelist" :depends-on ("package"))
   (:file "filesystem-viewer" :depends-on ("treelist"))))
